
<!DOCTYPE html>
<html >
<meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
   <?php include 'layout/cssJsResource.php';?>
   <script type="text/javascript">

              // $('').newsTicker({
              //       row_height: 80,
              //       max_rows: 3,
              //       duration: 4000
              //   });

              jQuery(function($) {
                $('#nt-example1').newsTicker({
                    row_height: 72,
                    max_rows: 4,
                    duration: 4000
                });

                $(".navbar-nav li a").removeClass("menu_active");
            $("#menuAboutUsId").addClass("menu_active");


            });
   </script>
  
</head>
<body>

   <?php include 'layout/header.php';?>




<section class="blog-header section-paddingTop">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <h1 class="section-title">ABOUT US</h1>
                    </div>
                </div>
            </div>
        </section>
        <section class="blog-sidebar" style="color: #696969 !important">
            <div class="container">
                <div class="row" style="margin:0px">
                    <div class="col-xs-12 col-sm-12 col-md-3 ">
                        <div id="divBreadCrumbs" style="margin-top:10px;">
                            <a href="index.php"><img src="images/home-icon.png" style="margin-top:-5px;" height="12px"></a> / Corporate / About us</div>
                    </div>
                </div>
                <div class="row" style="margin:0px; padding: 0px">
                    <div class="col-xs-12 col-sm-12 col-md-3 ">
                        <div class="sidebar" style="padding: 0px 2px; ">
                            <div class="list-group" id="divServicemenu">
                                <a href="javascript:" class="list-group-item active">Corporate</a>
                                <a href="about-us.php" style="color: #195FA4; font-weight: bold;" class="list-group-item" style="padding-right:5px" >
                                    <i class="fa fa-user"></i> About us</a>
                                <a href="services.php" class="list-group-item" style="padding-right:5px">
                                    <i class="fa fa-cog icon-large"></i> Services</a>
                                                                    
                                  <a href="philosophy.php" class="list-group-item" style="padding-right:5px">
                                    <i class="fa fa-bullseye icon-large"></i> Philosophy</a>
                            </div>
                        </div>
                         <div class="thumbnail hidden-xs hidden-sm">
                                <img src="images/bearing_1.jpg" style="height: 160px; width: 100%" class="firstmscimage ">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-9 animated" >
                        <div class="blog-content">
                            <div class="sidebar-blog-content">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        <div>
                                            <p style="text-align: justify;">
                                               <b>Rotation Engineering Ltd</b>. is a Private Limited Company under the companies Act, 1994 (ACT XVIII OF 1994). The company is established in the year 2014.
                                            </p>
                                            <div class="row" style="padding-bottom: 0px">
                                                <div class="col-xs-12 col-sm-6 col-md-6">
                                                     <ul class="innerStepList">
                                                        <li>
                                                            <span>
                                                                <i class="fa fa-check" aria-hidden="true"></i>
                                                                <h5>Business Scope</h5>
                                                                <p>Importer.</p>
                                                                <p> Supplier of all types of industrial bearings.</p>
                                                                <p>Valves, lubricant, Generator</p>
                                                                <p>Hydraulic Brick Maker Machine from spinning dust</p>
                                                                <p>Dehumidification plant </p>
                                                                <p>Service Provider</p>
                                                               
                                                            </span>
                                                        </li>
                                                        </ul>
                                                </div>
                                                <div class="col-xs-12 col-sm-6 col-md-6">
                                                <ul class="innerStepList">
                                                <li>
                                                            <span>
                                                                <i class="fa fa-check" aria-hidden="true"></i>
                                                                <h5>Distributorship</h5>  
                                                                <p>MTK Bearing-Belgium.</p>
                                                                <p>Brugarolus Lubricant-Spain .</p>
                                                                <p>Temsan Air Engineering-Turkey.</p>
                                                                <p>Paikane Genset-India</p>
                                                            </span>
                                                        </li>
                                                        
                                                       
                                                    </ul>
                                                </div>
                                            </div>

                                                
                                            <p style="text-align: justify;">
                                                <b>Rotation Engineering Ltd</b> (REL) is the authorized distributor of MTK+ (Motion Technology Knowledge) brand industrial bearings the country of origin is EU and all bearing shipped from Belgium. MTK is a renowned global bearing manufacturing company in every continent and an international reputation for excellence in the design and manufacture of industrial, marine, locomotive, automobile bearings such as Deep groove ball bearing, Snap ring ball bearing, Self-aligning ball bearings, Tapered roller bearings, Spherical roller bearings, Thrust roller bearings, Needle Bearings, Insert Ball bearings, Spherical plain bearings  etc. REL also provide all type of BRUGAROLAS lubricant from ready stock and indent basis. We provide dehumidification plant and hydraulic brick maker machine as TEMSAN brand-Turkey.
                                            </p>
                                            <p style="text-align: justify;  padding-top: 10px">
                                                We maintain available stock of bearing of various model, sizes and applications in our office store. We have no showroom or dealer in Bangladesh.
                                            </p>
                                            <p style="text-align: justify;  padding-top: 10px">
                                                Our bearings are running satisfactorily for different application in different industries around the country with 100% customer satisfaction.
                                            </p>
                                            <p style="text-align: justify; padding-top: 10px">
                                                <b>Rotation Engineering Ltd</b> started to become well-known organization in the sector of supplying genset with two world renowned brand <b>Visa energy (UK) and Paikane Energy (India)</b>. Our generator powered by Cummins(UK), Volvo (Sweden), Perkins(UK), Deutz (Germany), MTU (Germany), Iveco-FPT(Italy), Mitsubishi (Japan), Lombardini (Italy) Etc: engine and  Alternator from world famous Stamford (UK), Mecc-Alte (Italy/UK) and control system from Power Cummins(UK), Deep sea Electronics(UK) , Comap (Czech), including installation, commissioning after sales service. We also provide installation and commissioning generator set other than our brand.
                                            </p>
                                            <p style="text-align: justify; padding-top: 10px">
                                                Besides supplying, installation, commissioning, after sales service we sell spare parts for both diesel & gas genset of all brand,  Automatic Transfer Panel, Automatic synchronized panel for diesel to diesel generator , Diesel to gas generator, gas to gas generator,  Generator controller. With excellent product quality, excellent support for technical solutions, quick attentive service support, competitive prices of spare parts and service charge, our company in the time of satisfy user’s requirement and also bring a feeling of value buy experience. We have a team with 20 years of experience in power generation, marine and industrial engines like Cummins, Caterpillar, Komatsu, Volvo, Detroit Diesel, Perkins, Deutz, MAN, John Deere etc.
                                            </p>
                                            <p style="text-align: justify; padding-top: 10px">
                                                Rotation looks forward to working with you in good faith, mutual benefit and win-win situation, altogether create happy tomorrow. 
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <!-- footer -->
        <?php include 'layout/footer.php';?>
        <!-- /Footer -->
       
  

   
</body>

</html>
