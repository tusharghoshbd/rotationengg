
<!-- https://www.oxagile.com/ -->

<!DOCTYPE html>
<html >
<meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
   <?php include 'layout/cssJsResource.php';?>
   <style type="text/css">
       
   </style>
   <script type="text/javascript">


              jQuery(function($) {
                $('#nt-example1').newsTicker({
                    row_height: 72,
                    max_rows: 4,
                    duration: 4000
                });

                $(".navbar-nav li a").removeClass("menu_active");
                $("#menuHomeId").addClass("menu_active");


            });
   </script>
  
</head>
<body>

   <?php include 'layout/header.php';?>

<div id="demo-1" data-zs-src='["Upload/Banners/8266426033_c8c830e351_h.jpg", 
"Upload/Banners/20622697_10207605981107287_1652677929_o.jpg",
"Upload/Banners/20615308_10207605981027285_1149467898_o.jpg",
"Upload/Banners/8266429353_d07ce461b6_o_c.jpg", 
"Upload/Banners/19243679_10207291190917729_1734707566.jpg"]' data-zs-overlay="dots">
    </div>


  <section id="about" class=" section-padding">
            <div class="container">
                <div class="row">
                    <div class="  col-xs-12 col-sm-6 col-md-3 text-center" >
                       <!--  <div class="icon-circle imgexpertteam">
                        </div> -->
                        <div style="padding-bottom: 10px; text-align: left;"><h2>Bearing For Industry</h2></div>
                        <div class="thumbnail">
                                <img src="images/bearing_1.jpg" style="height: 160px; width: 100%" class="firstmscimage ">
                        </div>
                        <div class="who-we-are-content text-center">
                            <p>MTK+ Bearings are used in many applications in a wide range of industries such as general engineering, general machinery, crane industry, steel industry, offshore industry and more.</p>
                        </div>
                    </div>
                    <div class="  col-xs-12 col-sm-6 col-md-3 text-center">
                        <div style="padding-bottom: 10px; text-align: left;"><h2>Engineering</h2></div>
                        <div class="thumbnail">
                                <img src="images/bearing_2.jpg" style="height: 160px; width: 100%" class="firstmscimage ">
                        </div>
                        <div class="who-we-are-content text-center">
                            <p>Our Application Engineering department can also make bearings according to your drawing. Send your inquiries with drawing and we will try to make you an offer as soon as possible.</p>
                        </div>
                    </div>
                    <div class="  col-xs-12 col-sm-6 col-md-3 text-center">
                        <div style="padding-bottom: 10px; text-align: left;"><h2>Distribution</h2></div>
                        <div class="thumbnail">
                                <img src="images/bearing_3.jpg" style="height: 160px; width: 100%" class="firstmscimage ">
                        </div>
                        <div class="who-we-are-content text-center">
                            <p>MTK+ Bearings offers you a wide range of distribution areas from Australia to Germany, South Africa to China. With a range of over 60 countries, we offer you a world-wide delivery option.</p>
                        </div>
                    </div>
                    <div class="  col-xs-12 col-sm-6 col-md-3 text-center">
                        <div style="padding-bottom: 10px; text-align: left;"><h2>Latest News</h2></div>
                
                        <ul id="nt-example1" style="height: 200px; overflow: hidden; text-align: left">
                            
                            <li ><i class="fa fa-arrow-right" ></i> 
                                MTK+ Bearing Factory NV/SA is a Belgium based manufacturing company, founded in 2005 as the result of a joint-venture between EMS INTERNATIONAL Company and the management of a bearing factory in Romania. 
                                <a href="#">Read more...</a> 
                            </li>
                            <li ><i class="fa fa-arrow-right" ></i> 
                                In 1983 the line of special lubricants known as BESLUX is launched in the marketplace. A line of products that are highlighted by their innovation and quality. The Beslux products continue to be an item of reference to this day. 
                                <a href="#">Read more...</a>
                            </li>
                            <li ><i class="fa fa-arrow-right" ></i> 
                                The Persevering Lathe Workshop is established. Perseverance Lathe symbolizes the high dreams of a determined craftsman in a tiny workshop in Turkey that day. 
                                <a href="#">Read more...</a> 
                            </li>
                            <li ><i class="fa fa-arrow-right" ></i> 
                                Pai Kane group is a reputed group based in Goa, engaged in manufacturing diverse engineering products. It commenced operations in 1971. 
                                <a href="#">Read more...</a>
                            </li>
                            <li ><i class="fa fa-arrow-right" ></i> 
                                Curabitur porttitor ante eget hendrerit adipiscing. Maecenas at magna. 
                                <a href="#">Read more...</a>\
                            </li>
                            <li ><i class="fa fa-arrow-right" ></i> 
                                Praesent ornare nisl lorem, ut condimentum lectus gravida ut. 
                                <a href="#">Read more...</a>
                            </li>
                            <li ><i class="fa fa-arrow-right" ></i> 
                                Nunc ultrices tortor eu massa placerat posuere. Vivamus viverra sagittis. 
                                <a href="#">Read more...</a>
                            </li>
                            <li ><i class="fa fa-arrow-right" ></i> 
                                Morbi sodales tellus sit amet leo congue bibendum. Ut non mauris eu neque. 
                                <a href="#">Read more...</a>
                            </li>

                        </ul>

                    </div>
                </div>
            </div>
        </section>



<section id="about" class="who-we-are section-padding" style="background-color: #f1f1f1">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-lg-12 text-center p-padding animated" >
                <h1 class="section-title">Distributorship</h1>
                <p style="padding-top: 20px">
                    Rotation Engineering Ltd (REL) is the authorized distributor of MTK+ (Motion Technology Knowledge) brand industrial bearings the country of origin is EU and all bearing shipped from Belgium. We are also distributor of Brugarolus Lubricant-Spain, Temsan Air Engineering-Turkey, Paikane Genset-India.
                </p>
            </div>

            <div class="wow zoomIn col-xs-12 col-sm-6 col-md-3 text-center animated" style="visibility: visible; animation-name: zoomIn;">
                <div class="icon-circle">
                <a href="http://www.mtk-bearings.com"  target="_blank"><img src="images/mtk-bearing.png" style="width: 120px; height: 100px; padding-bottom:10px;"></a>
                </div>
                <div class="who-we-are-content text-center">
                    <h2 style="text-align: center;"><a href="http://www.mtk-bearings.com"  target="_blank">MTK Bearing</a></h2>
                    <p>MTK+ Bearing Factory NV/SA is a Belgium based manufacturing company, founded in 2005 as the result of a joint-venture between EMS INTERNATIONAL Company and the management of a bearing factory in Romania. </p>
                </div>
            </div>
            <div class="wow zoomIn col-xs-12 col-sm-6 col-md-3 text-center animated" style="visibility: visible; animation-name: zoomIn;">
                <div class="icon-circle " style="height: 99px">
                    <a href="https://brugarolas.com"  target="_blank"><img src="images/brugarolas-logo.png" style="width: 150px; height: 55px; margin-top: 20px"></a>
                </div>
                <div class="who-we-are-content text-center">
                    <h2 style="text-align: center;"><a href="https://brugarolas.com"  target="_blank">Brugarolus Lubricant</a></h2>
                    <p>In 1983 the line of special lubricants known as BESLUX is launched in the marketplace. A line of products that are highlighted by their innovation and quality. The Beslux products continue to be an item of reference to this day.  </p>
                </div>
            </div>
            <div class="clearfix hidden-md hidden-lg"></div>
            <div class="wow zoomIn col-xs-12 col-sm-6 col-md-3 text-center animated" style="visibility: visible; animation-name: zoomIn;">
                <div class="icon-circle " style="height: 99px">
                    <a href="http://www.temsanmakina.com/"  target="_blank"> <img src="images/temsan-logo.jpg" style="height: 60px; margin-top: 10px"></a>
                </div>
                <div class="who-we-are-content text-center">
                    <h2 style="text-align: center;"><a href="http://www.temsanmakina.com/"  target="_blank">Temsan Air Engineering</a></h2>
                    <p>The Persevering Lathe Workshop is established. Perseverance Lathe symbolizes the high dreams of a determined craftsman in a tiny workshop in Turkey that day. </p>
                </div>
            </div>
            <div class="wow zoomIn col-xs-12 col-sm-6 col-md-3 text-center animated" style="visibility: visible; animation-name: zoomIn;">
                <div class="icon-circle" style=" height: 99px">
                    <a href="http://paikane.com" target="_blank"><img src="images/pai-kane.png" style=" height: 75px; width: 113px; margin-top: 10px"></a>
                </div>
                <div class="who-we-are-content text-center">
                    <h2 style="text-align: center;"><a href="http://paikane.com"  target="_blank">Paikane Genset</a></h2>
                    <p>Pai Kane group is a reputed group based in Goa, engaged in manufacturing diverse engineering products. It commenced operations in 1971.</p>
                </div>
            </div>
        </div>
    </div>
</section>



<section id="services" class="service section-padding">
    <div class="container">
        <div class="row">
            <div class=" col-xs-12 text-center p-padding">
                <h1 class="section-title">services</h1>
                <p>Our main services are scheduled maintenance, repair of damaged engine and alternators and supply of spare parts for diesel & gas generator. At the moment we serve a large number of customers who own engines and need something more than a mere repair of them, that is, the technical guarantee that all the systems work properly.  </p>
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5">
                <div class="left-column">
                    <div class="wow zoomIn media">
                        <div class="media-left media-middle">
                                <div class="imgwebdesign"></div>
                        </div>
                        <div class="media-body">
                                <h2>scheduled maintenance</h2>
                        </div>
                    </div>
                    <div class="wow zoomIn media">
                        <div class="media-left media-middle">
                                <div class="pull-left imgapidevelopment"></div>
                        </div>
                        <div class="media-body">
                                <h2>repair of damaged engine and alternators </h2>
                        </div>
                    </div>
                    <div class="wow zoomIn media">
                        <div class="media-left media-middle">
                                <div class="pull-left imgcustomize"></div>
                        </div>
                        <div class="media-body">
                                <h2>spare parts for diesel & gas generator</h2>
                        </div>
                    </div>
                    <div class="wow zoomIn media">
                        <div class="media-left media-middle">
                                <div class="pull-left imgecommercesolution"></div>
                        </div>
                        <div class="media-body">
                                <h2>Annual Service Contract </h2>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-6 col-sm-2 col-md-2 hidden-xs hidden-sm">
                <div class="image-box">
                    <img class="img-responsive" src="images/phn.png" alt="Key Concepts Surat">
                </div>
            </div>

            <div class="col-xs-12 col-sm-5 col-md-5">
                <div class="right-column">
                    <div class="wow zoomIn media">
                        <div class="media-left media-middle">
                                <div class="pull-left imgerpsolution"></div>
                        </div>
                        <div class="media-body">
                                <h2>Overhauling any kind of breakdowns</h2>
                        </div>
                    </div>
                    <div class="wow zoomIn media">
                        <div class="media-left media-middle">
                                <div class="pull-left imgodoo"></div>
                        </div>
                        <div class="media-body">
                                <h2>problem diagnosis for all kinds of engine</h2>
                        </div>
                    </div>
                    <div class="wow zoomIn media">
                        <div class="media-left media-middle">
                                <div class="pull-left imgandroidios"></div>
                        </div>
                        <div class="media-body">
                                <h2>offers generator for rented purpose </h2>
                        </div>
                    </div>
                    <div class="wow zoomIn media">
                        <div class="media-left media-middle">
                                <div class="pull-left imgdigitalmarketing"></div>
                        </div>
                        <div class="media-body">
                                <h2>Any technical Support</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="more-area">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h2 class="pull-left">
                    Download our Product List for offline information
                </h2>
                <a type="button" id="load" class="btn btn-success pull-right" target="_blank" href="Upload/MTK_BEARING_DATA.xls">Download</a>
            </div>
        </div>
    </div>
</div>



<section class="partner-wrapper section-padding">
    <div class="container">
        <div class="row">

            <div class=" col-sm-12">
                <h1 class="section-title">Major Clients</h1>
                <div class="owl-wrap">
                    <div id="owl-example" class="owl-carousel">

                                        
                                        
                                        <div class="feature-image"><img class="img-responsive" src="Upload/Clients/antim_logo_small.png" alt="Key Concepts Clients" style="margin-top: 25px"></div>

                                        <div class="feature-image"><img class="img-responsive" src="Upload/Clients/SM Group.jpg" alt="Key Concepts Clients" style="margin-top: 25px"></div>

                                        <!-- <div class="feature-image"><img class="img-responsive" src="Upload/Clients/Arunima Group.jpg" alt="Key Concepts Clients"></div> -->
                                        <div class="feature-image"><img class="img-responsive" src="Upload/Clients/Delta.png" alt="Key Concepts Clients" style="margin-top: 30px"></div>
                                        
                                           <div class="feature-image"><img class="img-responsive" src="Upload/Clients/Opex_Shina.png" alt="Key Concepts Clients"></div>
                                        <div class="feature-image"><img class="img-responsive" src="Upload/Clients/Akij.jpg" alt="Key Concepts Clients"></div>
                                        <div class="feature-image"><img class="img-responsive" src="Upload/Clients/AkoTex.jpg" alt="Key Concepts Clients"></div>

                                       


                                        <div class="feature-image"><img class="img-responsive" src="Upload/Clients/Badsha Tex.jpg" alt="Key Concepts Clients"></div>
                                        <div class="feature-image"><img class="img-responsive" src="Upload/Clients/Beximco.png" alt="Key Concepts Clients" style="margin-top: 20px"></div>
                                        
                                        <div class="feature-image"><img class="img-responsive" src="Upload/Clients/Pran RFl.jpg" alt="Key Concepts Clients" style="margin-top: 20px"></div>

                                         <div class="feature-image"><img class="img-responsive" src="Upload/Clients/Mosharof.png" alt="Key Concepts Clients"></div>
                                        

                                        <div class="feature-image"><img class="img-responsive" src="Upload/Clients/Aman.png" alt="Key Concepts Clients" style="height: 60px; margin-top: 10px"></div>

                                        <div class="feature-image"><img class="img-responsive" src="Upload/Clients/Shah Cement.png" alt="Key Concepts Clients" style="height: 60px; margin-top: 10px"></div>

                                        <div class="feature-image">
                                        <img class="img-responsive" src="Upload/Clients/amber.jpg" alt="Key Concepts Clients" style="height: 60px; margin-top: 10px" >
                                        </div>

                                        <div class="feature-image"><img class="img-responsive" src="Upload/Clients/Envoy.jpg" alt="Key Concepts Clients" style="height: 60px; margin-top: 10px"></div>

                                        
                                        <div class="feature-image"><img class="img-responsive" src="Upload/Clients/UnionGroup.png" alt="Key Concepts Clients" style="height: 60px; margin-top: 10px"></div>

                    </div>
                </div>

            </div>
        </div>
    </div>
</section>






        <!-- footer -->
        <?php include 'layout/footer.php';?>
        <!-- /Footer -->
       
  

   
</body>

</html>
