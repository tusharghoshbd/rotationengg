
<!DOCTYPE html>
<html >
<meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
   <?php include 'layout/cssJsResource.php';?>
   <script type="text/javascript">

              // $('').newsTicker({
              //       row_height: 80,
              //       max_rows: 3,
              //       duration: 4000
              //   });

              jQuery(function($) {
                $('#nt-example1').newsTicker({
                    row_height: 72,
                    max_rows: 4,
                    duration: 4000
                });

                $(".navbar-nav li a").removeClass("menu_active");
            $("#menuAboutUsId").addClass("menu_active");


            });
   </script>
  
</head>
<body>

   <?php include 'layout/header.php';?>

<section class="blog-header section-paddingTop">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <h1 class="section-title">SERVICES</h1>
                    </div>
                </div>
            </div>
        </section>
        <section class="blog-sidebar" style="color: #696969 !important">
            <div class="container">
                <div class="row" style="margin:0px">
                    <div class="col-xs-12 col-sm-12 col-md-3 ">
                        <div id="divBreadCrumbs" style="margin-top:10px;">
                            <a href="index.php"><img src="images/home-icon.png" style="margin-top:-5px;" height="12px"></a> / Corporate / Services</div>
                    </div>
                </div>
                <div class="row" style="margin:0px; padding: 0px">
                    <div class="col-xs-12 col-sm-12 col-md-3 ">
                        <div class="sidebar" style="padding: 0px 2px; ">
                            <div class="list-group" id="divServicemenu">
                                <a href="javascript:" class="list-group-item active">Corporate</a>
                                <a href="about-us.php" class="list-group-item" style="padding-right:5px" >
                                    <i class="fa fa-user"></i> About us</a>
                                <a href="services.php"  style="color: #195FA4; font-weight: bold;" class="list-group-item" style="padding-right:5px">
                                    <i class="fa fa-cog icon-large"></i> Services</a>
                                                                    
                                <a href="philosophy.php" class="list-group-item" style="padding-right:5px">
                                    <i class="fa fa-bullseye icon-large"></i> Philosophy</a>
                            </div>
                        </div>
                        <div class="thumbnail hidden-xs hidden-sm">
                                <img src="images/bearing_1.jpg" style="height: 160px; width: 100%" class="firstmscimage ">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-9 animated" >
                        <div class="blog-content">
                            <div class="sidebar-blog-content">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                            <ul class="innerStepList" style="text-align: left; padding: 0px; margin:0px ">
                                                <li style="margin: 0px; padding: 0px">
                                                <span>
                                                    <i class="fa fa-check" aria-hidden="true"></i>
                                                    <h5>Services</h5>
                                                </span>
                                                </li>
                                            </ul>
                                            <p style="text-align: justify;">
                                              <b>Our main services</b> are scheduled maintenance, repair of damaged engine and alternators and supply of spare parts for diesel & gas generator. 
                                               </p>
                                                <p style="text-align: justify;  padding-top: 10px">
                                                At the moment we serve a large number of customers who own engines and need something more than a mere repair of them, that is, the technical guarantee that all the systems work properly. 
                                                </p>
                                                 <p style="text-align: justify;  padding-top: 10px">
                                                The information processing gotten from the Engines Management programmes is our specialty. This, together with our extensive experience in the field of mechanics makes us an independent and fully competitive company.
                                                </p>
                                                 <p style="text-align: justify;  padding-top: 10px">
                                                We also offer Annual Service Contract, Overhauling any kind of breakdowns, problem diagnosis for all kinds of engine & alternator. Rotation Engineering Ltd also offers generator for rented purpose of ranges for the power ranges 200KVA to 1200KVA.

                                            </p>
                                            <br/><br/>
                                            <ul class="innerStepList" style="text-align: left; padding: 0px; margin:0px ">
                                                <li style="margin: 0px; padding: 0px">
                                                <span>
                                                    <i class="fa fa-check" aria-hidden="true"></i>
                                                    <h5>Spare Parts</h5>
                                                </span>
                                                </li>
                                            </ul>
                                            <p style="text-align: justify;  padding-top: 10px">
                                                <b>Rotation</b> have a wide variety of spare parts for diesel engine such as DEUTZ, CUMMINS, PERKINS, JOHN DEERE, VOLVO, IVECO, LOMBARDINI, DETROIT DIESEL, COMATSU, MTU, YANMAR and Gas engine such as DEUTZ- MWM, CUMMINS, CATER PILLAR and variety of AVR and controller in stock.
                                            </p>
                                            <br/><br/>
                                             <ul class="innerStepList" style="text-align: left; padding: 0px; margin:0px ">
                                                <li style="margin: 0px; padding: 0px">
                                                <span>
                                                    <i class="fa fa-check" aria-hidden="true"></i>
                                                    <h5>Facilities</h5>
                                                </span>
                                                </li>
                                            </ul>
                                             <p style="text-align: justify;  padding-top: 10px">
                                                <b>Our workshop</b> facilities are equipped with standard equipment of tools for optimum performance of the required generators that enable us to do quality works.
                                                </p>
                                                 <p style="text-align: justify;  padding-top: 10px">
                                                We look forward to explore a business opportunity with you and thank you for taking the time to learn more about us.

                                            </p>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>



        <!-- footer -->
        <?php include 'layout/footer.php';?>
        <!-- /Footer -->
       
  

   
</body>

</html>
