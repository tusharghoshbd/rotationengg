<?php require 'function.php'; ?>

<?php 

    $conn = db_connection();

    $sql = "SELECT * FROM bearing_category";
    $result = $conn->query($sql);
    $resultArr  = array();
    $resBearing = array();


        $i = 0;
        while($row = $result->fetch_assoc()) {
             $resultArr[$i]= $row;
             $i++;
        }

    //bearign data
    $sql = "";
    if(!empty($_GET["pcat"]) && isset($_GET['pcat']))
         $sql = "SELECT * FROM bearing_data where category_id=".$_GET['pcat'];
     else
         $sql = "SELECT * FROM bearing_data";

    $result = $conn->query($sql);
    $resBearing = array();

        $i = 0;
        while($row = $result->fetch_assoc()) {
             $resBearing[$i]= $row;
             $i++;
        }

    //breadcrum
        $breadcrum ="";
    if(!empty($_GET["pcat"]) && isset($_GET['pcat'])){
         $sql = "SELECT * FROM bearing_category where id=".$_GET['pcat'];
         $result = $conn->query($sql);
         while($row = $result->fetch_assoc()) {
             $breadcrum= $row['category_name'];
        }
     }
     else
         $breadcrum = 'All Products';

    //print_r( $resultArr);

    $conn->close();



?>

<!DOCTYPE html>
<html >
<meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
   <?php include 'layout/cssJsResource.php';?>



   <script type="text/javascript">

              // $('').newsTicker({
              //       row_height: 80,
              //       max_rows: 3,
              //       duration: 4000
              //   });

              jQuery(function($) {
                $('#nt-example1').newsTicker({
                    row_height: 72,
                    max_rows: 4,
                    duration: 4000
                });

                $(".navbar-nav li a").removeClass("menu_active");
                $("#menuProductsId").addClass("menu_active");


                $('#dynamic-table').DataTable( {
                    bAutoWidth: false,
                    // "aoColumns": [
                    //   { "bSortable": false },
                    //   null, null,null, null, null,
                    //   { "bSortable": false }
                    // ],
                    "aaSorting": [],
                    "lengthMenu": [[50, 80, 100,300], [50, 80, 100, 300]],
            
                    select: {
                        style: 'multi'
                    }
                } );


            });
   </script>
  
</head>
<body>

   <?php include 'layout/header.php';?>


        <section class="blog-header section-paddingTop">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <h1 class="section-title">Products</h1>
                    </div>
                </div>
            </div>
        </section>
        <section class="blog-sidebar" style="color: #696969 !important">
            <div class="container">
                <div class="row" style="margin:0px">
                    <div class="col-xs-12 col-sm-12 col-md-12 ">
                        <div id="divBreadCrumbs" style="margin-top:10px;">
                            <a href="index.php"><img src="images/home-icon.png" style="margin-top:-5px;" height="12px"></a> / Products / <?php  echo ucwords(strtolower($breadcrum)); ?> </div>
                    </div>
                </div>
                <div class="row" style="margin:0px; padding: 0px">
                    <div class="col-xs-12 col-sm-12 col-md-4 ">
                        <div class="sidebar" style="padding: 0px 2px; ">
                            <div class="list-group" id="divServicemenu">
                                <a href="javascript:" class="list-group-item active">Products Category</a>
                                <a href="products.php"  
                                <?php
                                    if(empty($_GET["pcat"]) && !isset($_GET['pcat']))
                                            echo 'style="color: #195FA4; font-weight: bold;"';
                                ?>
                                class="list-group-item" style="padding-right:5px" > 
                                            <i class="fa fa-arrow-circle-right icon-large"></i> 
                                    All Products
                                            </a>
                                <?php 
                                    for($i=0; $i<count($resultArr); $i++){

                                        echo ' <a href="products.php?pcat='.$resultArr[$i]['id'].'"';

                                            if(!empty($_GET["pcat"]) && isset($_GET['pcat']))
                                                if($_GET["pcat"] == $resultArr[$i]['id']){

                                                    echo 'style="color: #195FA4; font-weight: bold;"';
                                                }

                                        echo 'class="list-group-item" style="padding-right:5px" > 
                                            <i class="fa fa-arrow-circle-right icon-large"></i> 
                                            '.ucwords(strtolower($resultArr[$i]['category_name'])).'
                                            </a>';
                                    }
                                ?>
                               

                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-8 animated" >
                        <div class="blog-content">
                            <div class="sidebar-blog-content">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12">

                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="table-header" style="background-color: #195FA4; padding: 5px; color: #ffffff">
                                            Product List
                                        </div>
                                        <div class="table-responsible" style=" background-color: #cbcbcb">
                                            <table id="dynamic-table" class="table table-striped table-bordered table-hover">
                                                <thead style="background-color: white; color: #48483d">
                                                    <tr>
                                                        <th>DESIGNATION</th>
                                                        <th>d(mm)</th>
                                                        <th>D(mm)</th>

                                                        <th> B(mm) </th>
                                                        <th>Weight(kg)</th>

                                                    </tr>
                                                </thead>

                                                <tbody>

                                                <?php

                                                    for($i=0; $i<count($resBearing); $i++){

                                                        echo '<tr>
                                                                <td width="50%">'.$resBearing[$i]['designation'].'</td>
                                                                <td style="text-align:center">'.$resBearing[$i]['d_mm'].'</td>
                                                                <td style="text-align:center">'.$resBearing[$i]['cap_d_mm'].'</td>
                                                                <td style="text-align:center">'.$resBearing[$i]['b_mm'].'</td>
                                                                <td style="text-align:center">'.$resBearing[$i]['net_weight'].'</td>
                                                            </tr>';
                                                    }
                                                ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <!-- footer -->
        <?php include 'layout/footer.php';?>
        <!-- /Footer -->
       
  

   
</body>

</html>
