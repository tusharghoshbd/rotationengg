<?php 

  session_start();
  if(isset($_POST['submit'])) {
      
      $email_to = "info@rotationengg.com";
      $email_subject = "This message comes from contact page!!!";
  
        $_SESSION["error"] ="";
        $_SESSION["message"] = "";
        $error_message = "";
        
        // validation expected data exists
        if(!isset($_POST['first_name']) ||
            !isset($_POST['last_name']) ||
            !isset($_POST['email']) ||
            !isset($_POST['message'])) {
            $error_message .='We are sorry, but there appears to be a problem with the form you submitted.<br />';     
        }

        $first_name = $_POST['first_name']; // required
        $last_name = $_POST['last_name']; // required
        $email_from = $_POST['email']; // required
        $message = $_POST['message']; // required
 
        
        $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';

        if(!preg_match($email_exp,$email_from)) {
          $error_message .= 'The Email Address you entered does not appear to be valid.<br />';
        }
       
          $string_exp = "/^[A-Za-z .'-]+$/";
       
        if(!preg_match($string_exp,$first_name)) {
          $error_message .= 'The First Name you entered does not appear to be valid.<br />';
        }
       
        if(!preg_match($string_exp,$last_name)) {
          $error_message .= 'The Last Name you entered does not appear to be valid.<br />';
        }
       
        if(strlen($message) < 2) {
          $error_message .= 'The Comments you entered do not appear to be valid.<br />';
        }
       
        if(strlen($error_message) > 0) {
             $_SESSION["error"] = $error_message;
        }
        else{
            $email_message = "Form details below.\n\n";

            function clean_string($string) {
              $bad = array("content-type","bcc:","to:","cc:","href");
              return str_replace($bad,"",$string);
            }
    
            $email_message .= "First Name: ".clean_string($first_name)."\n";
            $email_message .= "Last Name: ".clean_string($last_name)."\n";
            $email_message .= "Email: ".clean_string($email_from)."\n";
            $email_message .= "Message: ".clean_string($message)."\n";
    
            $headers = 'From: '.$email_from."\r\n".
            'Reply-To: '.$email_from."\r\n" .
            'X-Mailer: PHP/' . phpversion();
            
            mail($email_to, $email_subject, $email_message);   
            $_SESSION["message"] = "Thank you for contacting us. We will be in touch with you very soon.";
            //echo  $_SESSION["message"];
        }
        
        header("Location: contact.php");
        exit;
  }
?>


<!DOCTYPE html>
<html >
<meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
   <?php include 'layout/cssJsResource.php';?>
   <script type="text/javascript">

              // $('').newsTicker({
              //       row_height: 80,
              //       max_rows: 3,
              //       duration: 4000
              //   });

              jQuery(function($) {
                $('#nt-example1').newsTicker({
                    row_height: 72,
                    max_rows: 4,
                    duration: 4000
                });

                 $(".navbar-nav li a").removeClass("menu_active");
                $("#menuContactId").addClass("menu_active");


            });
   </script>
  
</head>
<body>

   <?php include 'layout/header.php';?>

    <section class="blog-header section-paddingTop">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <h1 class="section-title">Contact</h1>
                    </div>
                </div>
            </div>
        </section>


<section class="blog-sidebar" style="color: #696969 !important">
    <div class="container">
        <div class="row" style="margin:0px">
            <div class="col-xs-12 col-sm-12 col-md-3 ">
                <div id="divBreadCrumbs" style="margin-top:10px;">
                    <a href="index.php"><img src="images/home-icon.png" style="margin-top:-5px;" height="12px"></a> / Contact</div>
            </div>
        </div>
        <div class="row" >
            <div class="col-xs-12 col-sm-12 col-md-12 ">
                <div class="container">
                    
                    <div class="row">
                        <div class="col-xs-12 col-sm-8 col-md-8">

                              <!--   <h2 class="text-center quick-contact-title">Quick Contact</h2>
                                <div class="row text-center quick-contact-descr">
                                    <div class="">
                                        <p>To get your project underway, simply contact us and an expert will get in touch with you as soon as possible.</p>
                                    </div>
                                </div> -->
                                    <br/>
<p>
                        <?php 
                            if(isset($_SESSION["message"]) &&  $_SESSION["message"] != "" ){
                              echo "<span style='color:green'> <b>".$_SESSION["message"]."</b></span>"; 
                              session_unset(); 
                            }
                            else if( isset($_SESSION["error"]) &&  $_SESSION["error"] != "" ){
                              echo "<span style='color:red'><b>".$_SESSION["error"]."</b></span>"; 
                              session_unset(); 
                            }
                        ?>
                      </p>
                                    <br/>

                            <div role="form" class="wpcf7" id="wpcf7-f8681-o1" dir="ltr" lang="en-US">
                                <div class="screen-reader-response"></div>
                                <form action="contact.php" method="post" class="wpcf7-form contact-form" novalidate="novalidate" id="cf7Form">

                                    <div class="row">
                                        <div class="col-md-6 form-col-left-md">
                                            <div class="form-group form-group-required">
                                                    <input name="first_name"  class="wpcf7-form-control wpcf7-text ed form-control" required="required"  placeholder="First Name" type="text">
                                            </div>
                                        </div>
                                        <div class="col-md-6 form-col-right-md">
                                            <div class="form-group form-group-required">
                                                    <input name="last_name"  class="wpcf7-form-control wpcf7-text ed form-control"  required="required" placeholder="Last Name" type="text">
                                            </div>
                                        </div>
                                    </div>
                                     <div class="row">
                                        <div class="col-md-12 form-col-left-md">
                                            <div class="form-group form-group-required">
                                                    <input name="email"  class="wpcf7-form-control wpcf7-text ed form-control" required="required"  placeholder="Email" type="text">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group form-group-required mee-char">

                                        <textarea name="message" id="message" cols="40" rows="6" maxlength="2000" class="wpcf7-form-control wpcf7-textarea wpcf7-validates-as-required form-control mee-char-input" aria-required="true" aria-invalid="false" placeholder="How can we help you?"></textarea>

                                    </div>
                                    <div class="text-center">
                                            <input value="Send" name="submit" class="btn btn-sm btn-primary btn-block contact-form-submit" type="submit">
                                    </div>
                                </form>
                            </div>

                            <br/><br/>
                        </div>

                        <div class="col-xs-12 col-sm- col-md-4">
                         <br/><br/>
                            <ul class="innerStepList" style="text-align: left; padding: 0px; margin:0px ">
                                                <li style="margin: 0px; padding: 0px">
                                                <span>
                                                    <i class="fa fa-check" aria-hidden="true"></i>
                                                    <h5>Address</h5>
                                                </span>
                                                </li>
                                            </ul>

                                            <p style="text-align: left;">
                                              

        House 18, Sonargaon Janopath <br/>
        Sector 11, Uttara, Dhaka, Bangladesh <br/>
        <b>Contact :</b> +880258955042, +8801990794054 <br/>
        <b>Email :</b> <a href="mailto:info@rotationengg.com"> info@rotationengg.com</a> <br/>


                                               </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


        <!-- footer -->
        <?php include 'layout/footer.php';?>
        <!-- /Footer -->
       
  

   
</body>

</html>
