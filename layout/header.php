<nav id="menu" class="mm-menu mm-horizontal mm-offcanvas">
        <ul class='mm-list mm-panel mm-opened mm-current' id='mm-0'>
            <li><a class='page-scroll' href='index.php'> Home</a></li>
            <li><a href='#mm-1' class='mm-subopen'></a><a href='about-us.php'>Corporate</a></li>
            <ul class='mm-list mm-panel mm-hidden' id='mm-1'>
                <li class='mm-subtitle'><a href='#mm-0' class='mm-subclose'>Corporate</a></li>
                <li><a href='about-us.php'> About Us </a></li>
                <li><a href='services.php'> Services </a></li>
                <li><a href='philosophy.php'> Philosophy </a></li>
            </ul>
            <li><a class='page-scroll' href='products.php?pcat=1'> Products</a></li>
            <li><a class='page-scroll' href='contact.php'> Contact</a></li>
            
        </ul>;
    </nav>

    <div class="main" id="home">
        <header class="header-part">
            <div style="background-color:#195fa4;color:#fff;line-height:18Px;padding:3px 0;font-size:12Px !important;">
                <div class="container">
                    <div class="col-lg-6">
                        <div class="pull-left" style="padding-right:20px;">
                            <i class="fa fa-phone"></i>&nbsp;&nbsp;+880258955042
                        </div>   
                        <div class="pull-left" style="padding-right:20px;">
                            <i class="fa fa-mobile"></i>&nbsp;&nbsp;+8801990794054
                        </div> 
                        <div class="pull-left">
                            <i class="fa fa-envelope"></i>&nbsp;&nbsp;<a href="mailto:info@rotationengg.com" style="color:#fff;">info@rotationengg.com</a>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="col-lg-6">
                        <div class="pull-right">
                            <a href="#" target="_blank"><i class="fa fa-facebook social-icons"></i></a>
                            <a href="#" target="_blank"><i class="fa fa-twitter social-icons"></i></a>
                            <a href="#" target="_blank"><i class="fa fa-google-plus social-icons"></i></a>
                            <a href="#" target="_blank"><i class="fa fa-linkedin social-icons"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div id="Div1" class="wrapper">
                <!-- Fixed navbar -->
                <div class="navi navbar-default" role="navigation" style="background-color: white; box-shadow: 0 0 3px 0 rgba(0, 0, 0, 0.41)">
                    <div class="container">
                        <div class="navbar-header page-scroll col-lg-2">
                            <a href="#menu">
                                <button id="headerBtnId" type="button" data-effect="st-effect-1" class="navbar-toggle collapsed" data-toggle="collapse" style="margin-top: 30px" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </a>
                            <a class="navbar-brand" href="index.php">
                               
                             <!--     <img class="visible-xs" src="images/19244303_10207289759121935_1910356559_n.png" />
                                    -->

                                    <img   id="logoid" class="logo-non-fixed" src="images/19758207_10207420832878697_1223890077_n.png"  style="max-height:90Px;" > 
                            </a>
                        </div>
                        <div id="navbar" class="navbar-collapse collapse pull-right hidden-xs">
                            <ul class="nav navbar-nav navbar-right">
                                <li><a id="menuHomeId" class="page-scroll menuLevel1 " href="index.php"> Home</a></li>
                                <li class="dropdown" onmouseenter='topMenuEnter(this);'><a id="menuAboutUsId" class="page-scroll   drop dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" href="about-us.php">Corporate</a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li class="menuLevel2"><a href="about-us.php">About Us</a></li>
                                        <li class="menuLevel2"><a href="services.php">Services</a></li>
                                        <li class="menuLevel2"><a href="philosophy.php">Philosophy</a></li>
                                    </ul>
                                </li>
                               
                                <li><a id="menuProductsId" class="page-scroll menuLevel1" href="products.php?pcat=1">Products</a></li>
                                
                                <!-- <li><a class="page-scroll menuLevel1" href="Career.html">Career</a></li> -->
                                <li><a id="menuContactId" class="page-scroll menuLevel1" href="contact.php">Contact</a></li>
                            </ul>
                        </div>

                        <div style="clear:both;"></div>
                    </div>
                </div><!-- End of Nav -->
            </div>
        </header>
        
<style>
    .carousel-inner-img {
        border: none;
        border-radius: 5px !important;
        box-shadow: 0 0 2px 2px #c2c2c2;
        height: auto !important;
        margin: 3px 0 28px 10px;
        width: 100% !important;
    }

    .tag-line {
        font-size: 26px !important;
    }

</style>