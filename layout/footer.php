 <footer>
            <div class="footer-wrapper section-padding">
                <div class="container">
                    <div class="row">

                        <div id="divFooterMenu2" class=" col-xs-12 col-sm-4 col-md-4 " style="visibility: visible; animation-name: zoomIn;">
                            <p class="footer-heading">Rotation Engineering Ltd</p>
                            <ul class="footermenu" style="column-count: 2 ; -webkit-column-count: 2;
    -moz-column-count: 2;">
                                    <li><a href="index.php"> Home </a></li>
                                    <li><a href="about-us.php"> About us</a></li>
                                    <li><a href="services.php"> Services</a></li>
                                    <li><a href="philosophy.php"> Philosophy</a></li>

                                    <li><a href="products.php?pcat=1"> Products </a></li>
                                    <li><a href="contact.php"> Contact</a></li>
                                </ul>
                        </div>

                        <div id="divFooterMenu3" class=" col-xs-12 col-sm-4 col-md-4 ">
                            <p class="footer-heading">Distributorship</p>
                            <i class="flaticon-home78"></i>
                                 <ul class="footermenu">
                                    <li><a target="_blank" href="http://www.mtk-bearings.com/"> MTK Bearing-Belgium. </a></li>
                                    <li><a target="_blank" href="https://brugarolas.com/"> Brugarolus Lubricant-Spain </a></li>
                                    <li><a target="_blank" href="http://www.temsanmakina.com/"> Temsan Air Engineering-Turkey</a></li>
                                    <li><a target="_blank" href="http://paikane.com/"> Paikane Genset-India </a></li>
                                </ul>
                        </div>
                        
                        <div id="divFooterMenu4" class="col-xs-12 col-sm-4 col-md-4 " style="visibility: visible; animation-name: zoomIn;">
                            <p class="footer-heading">Address</p>
                        <div>
                            <ul class="footercontact">
                                <li>
                                <p style="line-height:20px; ">House  18, Sonargaon Janopath <br/>Sector 11, Uttara, Dhaka, Bangladesh</p>
                                </li>
                                <li><span>Contact : </span> +880258955042, +8801990794054</li>

                                <li><span>Email : </span> <a href="mailto:info@rotationengg.com">info@rotationengg.com</a>
                            </ul>
                            </div>
                            </div>
                    </div>
                </div>
            </div>
            <div class="footer-bottom">
                <div class="container">
                    <div class="row">
                        <div class=" col-xs-12">
                           <!--  <p id="divFooterMenu5_">© 2014 - 2017 All rights reserved by Rotation Engineering Ltd</p> -->
                            <div class="  pull-left">
                                © 2014 - 2017 All rights reserved by Rotation Engineering Ltd
                            </div>
                            <!-- 
                            <div class="  pull-right">
                                <a href="sampritiit.com">Sampriti IT Solutions</a>
                            </div> -->
                            <div class="backtop  pull-right">
                                <i class="fa fa-angle-up back-to-top"></i>
                            </div>
                            <div class="pull-right" style="margin-right: 50px">
                                Powered by: <a target="_blank" href="https://www.sampritiit.com">Sampriti IT</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- 
  <script src="Area/KCITS/Scripts/jsb276?v=Kc2UhzGjXpQOY0xWpjU9Zm6E-gO-28IJpEe1g-AcJJ81"></script> -->
        <!-- 
<script src="Scripts/root.js"></script> -->
    
   <script src="Scripts/jquery.zoomslider.min.js"></script>
   <script src="Scripts/root.js"></script>



        <script>


    $(window).on('scroll', function() {
        if ($(window).scrollTop() > 10) {
            $('.navbar-default').addClass('navbar-fixed-top');
            $('#logoid').removeClass('logo-non-fixed');
            $('#logoid').addClass('logo-fixed');
             $('#headerBtnId').css("margin-top",'15px')

            
            

        } else {
            $('.navbar-default').removeClass('navbar-fixed-top');
            $('#logoid').removeClass('logo-fixed');
            $('#logoid').addClass('logo-non-fixed');
            $('#headerBtnId').css("margin-top",'30px')
            
        }
    });
      
    function topMenuEnter(obj) {
        $("#navbar").find('ul>li.open').each(function(index, item) {
            $(this).removeClass('open');
        });
        $(obj).addClass('open');
    }



    </script>