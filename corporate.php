<!DOCTYPE html>
<html class="no-js" lang="en">
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<!-- /Added by HTTrack -->

<head>
    <?php include 'layout/cssJsResource.php';?>
</head>

<body>
    <?php include 'layout/header.php';?>

        <section class="blog-header section-paddingTop">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <h1 class="section-title">KCITS</h1>
                    </div>
                </div>
            </div>
        </section>
        <section class="blog-sidebar">
            <div class="container">
                <div class="row" style="margin:0px">
                    <div class="col-xs-12 col-sm-12 col-md-3 ">
                        <div id="divBreadCrumbs" style="margin-top:10px;">
                            <a href="/"><img src="/Areas/KCITS/Images/home-icon.png" style="margin-top:-5px;" height="12px"></a> / <a href="javascript:" class="breadCrumbs ">About</a> / <a href="javascript:" class="breadCrumbs breadCrumbsActive">Key Concepts</a> </div>
                    </div>
                </div>
                <div class="row" style="margin:0px; padding: 0px">
                    <div class="col-xs-12 col-sm-12 col-md-3 ">
                        <div class="sidebar" style="padding: 0px 2px; ">
                            <div class="list-group" id="divServicemenu">
                                <a href="javascript:" class="list-group-item active">About</a>
                                <a href="/Company-Profile" class="list-group-item" style="padding-right:5px">
                                    <i class="fa fa-home icon-large"></i> Key Concepts</a>
                                <style>
                                .imgSideMenu1 {
                                    background: url('/Upload/MenuItem/20155030.png') no-repeat scroll 0px 0px transparent;
                                    height: 25px;
                                    width: 25px;
                                    background-size: 224%;
                                }
                                
                                .imgSideMenu1:hover {
                                    background: url('/Upload/MenuItem/20155030.png') no-repeat scroll -29px 0px transparent;
                                    height: 25px;
                                    width: 25px;
                                    background-size: 224%;
                                }
                                </style>
                                <a href="/Testimonial" class="list-group-item" style="padding-right:5px">
                                    <i class="fa fa-home icon-large"></i> Testimonial</a>
                                <style>
                                .imgSideMenu2 {
                                    background: url('/Upload/MenuItem/20153263.png') no-repeat scroll 0px 0px transparent;
                                    height: 25px;
                                    width: 25px;
                                    background-size: 224%;
                                }
                                
                                .imgSideMenu2:hover {
                                    background: url('/Upload/MenuItem/20153263.png') no-repeat scroll -29px 0px transparent;
                                    height: 25px;
                                    width: 25px;
                                    background-size: 224%;
                                }
                                </style>
                                <a href="/Event" class="list-group-item" style="padding-right:5px">
                                    <i class="fa fa-home icon-large"></i> Events</a>
                                <style>
                                .imgSideMenu3 {
                                    background: url('/Upload/MenuItem/20152427.png') no-repeat scroll 0px 0px transparent;
                                    height: 25px;
                                    width: 25px;
                                    background-size: 224%;
                                }
                                
                                .imgSideMenu3:hover {
                                    background: url('/Upload/MenuItem/20152427.png') no-repeat scroll -29px 0px transparent;
                                    height: 25px;
                                    width: 25px;
                                    background-size: 224%;
                                }
                                </style>
                                <a href="/OurTeam" class="list-group-item" style="padding-right:5px">
                                    <i class="fa fa-home icon-large"></i> Our Team</a>
                                <style>
                                .imgSideMenu4 {
                                    background: url('/Upload/MenuItem/20151701.png') no-repeat scroll 0px 0px transparent;
                                    height: 25px;
                                    width: 25px;
                                    background-size: 224%;
                                }
                                
                                .imgSideMenu4:hover {
                                    background: url('/Upload/MenuItem/20151701.png') no-repeat scroll -29px 0px transparent;
                                    height: 25px;
                                    width: 25px;
                                    background-size: 224%;
                                }
                                </style>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-9 animated" style="padding-right:50px;">
                        <div class="blog-content">
                            <div class="sidebar-blog-content">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        <div>
                                            <p><span style="color:#FF6633;"><b><span style="font-variant: normal;">K</span></b>
                                                </span><span style="color:#626262;">ey</span>
                                                <font color="#000000"> <span style="color:#FF6633;"><b><span style="font-variant: normal;">C</span></b>
                                                    </span><span style="color:#626262;">oncepts</span></font><span style="color:#626262;">&nbsp;is a software / apps and website development and outsourcing company which has one central goal - “Customer satisfaction”.</span></p>
                                            &nbsp;
                                            <p>
                                                <font color="#626262">Years experience in providing offshore software development and project management has given us a space in new sky by locating our new office in USA. Our global application solution </font><span style="color:#626262;">provider</span>
                                                <font color="#626262"> model make sure that we deliver maximum targeted results to YOU.</font>
                                            </p>
                                            <ul style="list-style-type: inherit; margin-left: 40px;">
                                                <li style="display:list-item;"><span style="color:#626262;">Low cost</span></li>
                                                <li style="display:list-item;"><span style="color:#626262;">High quality</span></li>
                                                <li style="display:list-item;"><span style="color:#626262;">Offshore software services</span></li>
                                                <li style="display:list-item;"><span style="color:#626262;">Complete business solutions</span></li>
                                                <li style="display:list-item;"><span style="color:#626262;">Value for client money</span></li>
                                            </ul>
                                            <p>
                                                <font color="#626262">These Key fundamentals are making us stand strong in this global competitive IT industry.</font>
                                            </p>
                                            <p style="margin-bottom: 0cm; border: medium none; padding: 0cm; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; widows: 1; text-align: center;">
                                                <font color="#626262"><b><span style="color: rgb(255, 102, 51);">"We are best in providing best IT services and complete end to end support."</span></b>
                                                    <br> &nbsp;
                                                    <img alt="IT Service Company" src="/Upload/FCKImages/20151211-00120150.jpg" style="width: 100%;"> </font>
                                            </p>
                                            <p style="text-align: justify;">
                                                <font color="#626262"><span style="color:#626262;">We focus on highly qualitative, timely and cost-effective complete business solution which enhances the client business. We make your business global and world wide available. Our business specific and user centric engineering using new technologies give your business an edge over your competitors. We are serving all over globally. Our local, national or international clients love us because we provide them highly scalable and stable solution. We have our loyal clients in USA, Canada, Germany, Denmark and Australia from long.</span></font>
                                            </p>
                                            <div>
                                                <div class="col-md-6">
                                                    <div id="wrapper">
                                                        <div class="grid-block-container">
                                                            <div class="grid-block fade in">
                                                                <div class="caption">
                                                                    <h3 style="text-align: center;"><span style="font-size:18px;"><b>Our Mission</b></span></h3>
                                                                    <p style="text-align: center;"><span style="font-size:14px;">Continues Providing services to the client which embraces their business in a global world.</span></p>
                                                                </div>
                                                                <img alt="application solution provider surat" src="/Upload/FCKImages/20160104-01010606.jpg" style="width: 100%;padding-bottom:0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div id="wrapper">
                                                        <div class="grid-block-container">
                                                            <div class="grid-block fade in">
                                                                <div class="caption">
                                                                    <h3 style="text-align: center;"><span style="font-size:18px;"><b>Our Goal</b></span></h3>
                                                                    <p style="text-align: center;"><span style="font-size:14px;">Complete Client satisfaction is our&nbsp;key&nbsp;goal.</span></p>
                                                                </div>
                                                                <img alt="IT Services Surat" src="/Upload/FCKImages/20160104-01014164.jpg" style="width: 100%; padding-bottom: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <script type="text/javascript">
                                            $(document).ready(function() {
                                                $('.fade').hover(
                                                    function() {
                                                        $(this).find('.caption').fadeIn(250);
                                                    },
                                                    function() {
                                                        $(this).find('.caption').fadeOut(250);
                                                    }
                                                );
                                            });
                                            </script>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- footer -->
        <?php include 'layout/footer.php';?>
        <!-- /Footer -->
    </div>
    <script src="Area/KCITS/Scripts/jsb276?v=Kc2UhzGjXpQOY0xWpjU9Zm6E-gO-28IJpEe1g-AcJJ81"></script>
    
</body>
<!-- Mirrored from keyconcepts.co.in/ by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 13 May 2017 17:44:48 GMT -->

</html>
