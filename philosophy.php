
<!DOCTYPE html>
<html >
<meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
   <?php include 'layout/cssJsResource.php';?>
   <script type="text/javascript">

              // $('').newsTicker({
              //       row_height: 80,
              //       max_rows: 3,
              //       duration: 4000
              //   });

              jQuery(function($) {
                $('#nt-example1').newsTicker({
                    row_height: 72,
                    max_rows: 4,
                    duration: 4000
                });

                $(".navbar-nav li a").removeClass("menu_active");
            $("#menuAboutUsId").addClass("menu_active");


            });
   </script>
  
</head>
<body>

   <?php include 'layout/header.php';?>


        <section class="blog-header section-paddingTop">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <h1 class="section-title">Philosophy</h1>
                    </div>
                </div>
            </div>
        </section>
        <section class="blog-sidebar" style="color: #696969 !important">
            <div class="container">
                <div class="row" style="margin:0px">
                    <div class="col-xs-12 col-sm-12 col-md-3 ">
                        <div id="divBreadCrumbs" style="margin-top:10px;">
                            <a href="index.php"><img src="images/home-icon.png" style="margin-top:-5px;" height="12px"></a> / Corporate / Philosophy</div>
                    </div>
                </div>
                <div class="row" style="margin:0px; padding: 0px">
                    <div class="col-xs-12 col-sm-12 col-md-3 ">
                        <div class="sidebar" style="padding: 0px 2px; ">
                            <div class="list-group" id="divServicemenu">
                                <a href="javascript:" class="list-group-item active">Corporate</a>
                                <a href="about-us.php"  class="list-group-item" style="padding-right:5px" >
                                    <i class="fa fa-user"></i> About us</a>
                                <a href="services.php" class="list-group-item" style="padding-right:5px">
                                    <i class="fa fa-cog icon-large"></i> Services</a>
                                                                    
                                <a href="philosophy.php" style="color: #195FA4; font-weight: bold;" class="list-group-item" style="padding-right:5px">
                                    <i class="fa fa-bullseye icon-large"></i> Philosophy</a>
                            </div>
                        </div>
                        <div class="thumbnail hidden-xs hidden-sm">
                                <img src="images/bearing_1.jpg" style="height: 160px; width: 100%" class="firstmscimage ">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-9 animated" >
                        <div class="blog-content">
                            <div class="sidebar-blog-content">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        <div>

                                             <div class="row" style="padding-bottom: 0px">
                                                <div class="col-xs-12 col-sm-12 col-md-12">
                                                     <ul class="innerStepList" style="text-align: left; padding-top: -20px; padding: 0px; margin:0px ">
                                                        <li>
                                                            <span>
                                                                <i class="fa fa-check" aria-hidden="true"></i>
                                                                <h5>Company Philosophy</h5>
                                                                <p><b>Our Spirit:</b> Diligence, Loyalty, Endeavor.</p>
                                                                <p> <b>Our Philosophy: </b> Moderate, integrity management & coordinated development of enterprises, employees, suppliers, customers.</p>
                                                                <p> <b>Our Goal:</b> To be a model in industrial sector to achieve of customer satisfaction & respect</p>
                                                                <p> <b>Our Strategy:</b> Service & customer satisfaction first and profit is its logical consequence.</p>
                                                                <p> <b>Our Quality policy:</b> Excellence, Technical innovation, to customer-oriented and constantly enterprising, always exceeds customer expectations. </p>
                                                               
                                                            </span>
                                                        </li>
                                                        </ul>
                                                </div>
                                            </div>
                                           
                                            <div class="row" style="padding-bottom: 0px">
                                                <div class="col-xs-12 col-sm-6 col-md-6">
                                                     <ul class="innerStepList">
                                                        <li>
                                                            <span>
                                                                <i class="fa fa-check" aria-hidden="true"></i>
                                                                <h5>Oue Mission</h5>
                                                                <div style="font-size: 14px; text-align: justify;">
                                                                    We strive every day to carry out our mission in both large and small ways by providing our customers with quality products from trusted vendors and through services carried out by our attentive employees.
                                                                </div>
                                                               
                                                            </span>
                                                        </li>
                                                        </ul>
                                                </div>
                                                <div class="col-xs-12 col-sm-6 col-md-6">
                                                <ul class="innerStepList">
                                                <li>
                                                            <span>
                                                                <i class="fa fa-check" aria-hidden="true"></i>
                                                                <h5>Our Vision</h5>  
                                                                 <div style="font-size: 14px; text-align: justify;">
                                                                 Our vision is to build a solid, sustainable business where people are the most important asset and where employees, clients, supplier and shareholder prosper. We achieve this purpose through innovative systems and providing our customers with the highest quality products and services.
                                                                </div>
                                                            </span>
                                                        </li>
                                                        
                                                       
                                                    </ul>
                                                </div>
                                            </div>

                                              
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <!-- footer -->
        <?php include 'layout/footer.php';?>
        <!-- /Footer -->
       
  

   
</body>

</html>
